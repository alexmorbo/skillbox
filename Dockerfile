# Builder
FROM golang:1.16 AS build-env


# Make dir
RUN mkdir /app
WORKDIR /app

# Copy mods
COPY ./go.mod /app
COPY ./go.sum /app

# Download mods
RUN go mod download

# Copy app
COPY ./ /app

# Buld app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /app/pig

# Run tests
RUN go test ./...


# Clean
FROM alpine

# Make dir
RUN mkdir /app
WORKDIR /app

# Copy build app
COPY --from=build-env /app/pig /app/pig

# Make resources dir
RUN mkdir /app/resources

# Copy resources
COPY --from=build-env /app/resources/index.html /app/resources/index.html

# Expose port
EXPOSE 8000

# Start app
CMD /app/pig